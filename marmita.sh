#!/bin/bash
################################################################################
# Script to be used to provision a new environment at AWS EKS with tools
# Command to be ran:
# ./marmita.sh
################################################################################

# Inspired by https://disconnected.systems/blog/another-bash-strict-mode/
# Used for loudly errors
set -euo pipefail
trap 's=$?; echo >&2 "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR

# Annoucement =)
# Generate your own logo here: http://patorjk.com/software/taag/#p=display&f=Graffiti&t=Type%20Something%20
echo " "
echo -e '\033[1;33mWelcome to the Marmita Script version 0.1!\033[0m'
echo -e '\033[1;31m'
echo "██╗     ███████╗ ██████╗     ██╗███████╗ ██████╗  ██████╗ ██╗     ██╗ "
echo "██║     ██╔════╝██╔═══██╗    ██║██╔════╝██╔════╝ ██╔════╝ ██║     ██║ "
echo "██║     █████╗  ██║   ██║    ██║█████╗  ██║  ███╗██║  ███╗██║     ██║ "
echo "██║     ██╔══╝  ██║   ██║    ██║██╔══╝  ██║   ██║██║   ██║██║     ██║ "
echo "███████╗███████╗╚██████╔╝    ██║███████╗╚██████╔╝╚██████╔╝███████╗██║ "
echo "╚══════╝╚══════╝ ╚═════╝     ╚═╝╚══════╝ ╚═════╝  ╚═════╝ ╚══════╝╚═╝ "                                                                                                                          
echo -e '\033[0m'

echo -e '\033[1;31m'
echo "#############################################################################"
echo "# BE SURE THAT YOU ALREADY HAVE TERRAFORM, HELM AND EKSCTL INSTALLED!!!     #"
echo "#############################################################################"
echo -e '\033[0m'

# If any tool will not be installed, it fails!
if ! command -v eksctl > /dev/null; then
  echo >&2 "Eksctl is not installed - please review your tools!!!"
  exit 1
fi

if ! command -v helm > /dev/null; then
  echo >&2 "Helm is not installed - please review your tools!!!"
  exit 1
fi

if ! command -v terraform > /dev/null; then
  echo >&2 "Terraform is not installed - please review your tools!!!"
  exit 1
fi

if ! command -v aws > /dev/null; then
  echo >&2 "AWS CLI is not installed - please review your tools!!!"
  exit 1
fi

# Clean environment 
cd cluster && $(terraform destroy -auto-approve ; rm -f terraform.tfstate* ; rm -rf .terraform ; rm -f .terraform) || true && cd ..

# Time to create the cluster and some AWS roles and policies
cd cluster && terraform init && terraform apply -auto-approve
sleep 3s

# Export variables to be used later
export CLUSTER_NAME=$(terraform output -raw cluster_name) ; echo $CLUSTER_NAME || true
export AWS_ACCOUNT=$(terraform output -raw aws_account) ; echo $AWS_ACCOUNT || true
export VPC_ID=$(terraform output -raw vpc_id) ; echo $VPC_ID || true
export REGION=$(terraform output -raw region) ; echo $REGION || true
export HOSTED_ZONE_NAME=$(terraform output -raw aws_route53_name) ; echo $HOSTED_ZONE_NAME || true
export HOSTED_ZONE_IDENTIFIER=$(terraform output -raw aws_route53_zone_id) ; echo $HOSTED_ZONE_IDENTIFIER || true

# Update kubeconfig file to access k8s cluster
aws eks --region $(terraform output -raw region) update-kubeconfig --name $(terraform output -raw cluster_name)
cd ..

# Adding necessary Helm Repos
helm repo add eks https://aws.github.io/eks-charts
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo add gitlab https://charts.gitlab.io
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update

# Prepare for ALB Installation
eksctl utils associate-iam-oidc-provider --cluster $CLUSTER_NAME --region $REGION --approve

eksctl create iamserviceaccount \
  --cluster=$CLUSTER_NAME \
  --region=$REGION \
  --namespace=kube-system \
  --name=aws-load-balancer-controller \
  --attach-policy-arn=arn:aws:iam::$AWS_ACCOUNT:policy/AWSLoadBalancerControllerIAMPolicy \
  --override-existing-serviceaccounts \
  --approve

# Apply ALB charts
kubectl apply -k "github.com/aws/eks-charts/stable/aws-load-balancer-controller//crds?ref=master"
helm install aws-load-balancer-controller eks/aws-load-balancer-controller \
    --set clusterName=$CLUSTER_NAME \
    --set serviceAccount.create=false \
    --set region=$REGION \
    --set vpcId=$VPC_ID \
    --set serviceAccount.name=aws-load-balancer-controller \
    -n kube-system
sleep 2s

# Verify Chart
kubectl get pods --all-namespaces

# Apply External DNS charts
helm install external-dns \
  --set provider=aws \
  --set aws.zoneType=public \
  --set txtOwnerId=$HOSTED_ZONE_IDENTIFIER \
  --set domainFilters[0]=$HOSTED_ZONE_NAME \
  bitnami/external-dns -n kube-system
sleep 2s

# Verify Chart
kubectl get pods --all-namespaces

# Install GitLab Runner
kubectl create ns gitlab
helm install --namespace gitlab gitlab-runner -f utils/gitlab-runner/values.yaml gitlab/gitlab-runner

# Install Prometheus and Grafana
kubectl create ns tools
kubectl create ns leoieggli-app
helm install --namespace tools prometheus bitnami/kube-prometheus
helm install --namespace tools grafana grafana/grafana

# End message

echo -e '\033[1;31m'
echo "▓██   ██▓ ▒█████   █    ██    ▓█████▄  ██▓▓█████▄     ██▓▄▄▄█████▓ ▐██▌  ▐██▌  ▐██▌ " 
echo " ▒██  ██▒▒██▒  ██▒ ██  ▓██▒   ▒██▀ ██▌▓██▒▒██▀ ██▌   ▓██▒▓  ██▒ ▓▒ ▐██▌  ▐██▌  ▐██▌ " 
echo "  ▒██ ██░▒██░  ██▒▓██  ▒██░   ░██   █▌▒██▒░██   █▌   ▒██▒▒ ▓██░ ▒░ ▐██▌  ▐██▌  ▐██▌ " 
echo "  ░ ▐██▓░▒██   ██░▓▓█  ░██░   ░▓█▄   ▌░██░░▓█▄   ▌   ░██░░ ▓██▓ ░  ▓██▒  ▓██▒  ▓██▒ " 
echo "  ░ ██▒▓░░ ████▓▒░▒▒█████▓    ░▒████▓ ░██░░▒████▓    ░██░  ▒██▒ ░  ▒▄▄   ▒▄▄   ▒▄▄  " 
echo "   ██▒▒▒ ░ ▒░▒░▒░ ░▒▓▒ ▒ ▒     ▒▒▓  ▒ ░▓   ▒▒▓  ▒    ░▓    ▒ ░░    ░▀▀▒  ░▀▀▒  ░▀▀▒ " 
echo " ▓██ ░▒░   ░ ▒ ▒░ ░░▒░ ░ ░     ░ ▒  ▒  ▒ ░ ░ ▒  ▒     ▒ ░    ░     ░  ░  ░  ░  ░  ░ " 
echo " ▒ ▒ ░░  ░ ░ ░ ▒   ░░░ ░ ░     ░ ░  ░  ▒ ░ ░ ░  ░     ▒ ░  ░          ░     ░     ░ " 
echo " ░ ░         ░ ░     ░           ░     ░     ░        ░            ░     ░     ░    " 
echo " ░ ░                           ░           ░                                        " 
echo -e '\033[0m'
