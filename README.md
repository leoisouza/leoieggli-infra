This is a project for Kubernetes EKS environment automation with a CI/CD flow, that build and deploy an entire Kubernetes environment to support applications. The idea here is to have a fully-automated system, that creates all the EKS infrastructure with VPC, roles, ALB, etc and enable Gitlab Runner for the application repository ( **https://gitlab.com/leoisouza/leoieggli-app** ).

# How to start
![START](./utils/figures/tenor.gif)

The idea is to simplify the process more as possibly, running a single command that can configure everything. But, before start it, please, **configure this required tools at your environment**:

- [**Terraform**](https://www.terraform.io/downloads.html)
- [**AWS CLI**](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
- [**Eksctl**](https://docs.aws.amazon.com/eks/latest/userguide/eksctl.html)
- [**Helm**](https://helm.sh/docs/intro/install/)

With this tools installed, let's go ahead with the configuration.

## How to start it?

Be sure to be logged at AWS, using the **aws configure** command - at least, you must have this two informations to be able to login:

- **AWS_ACCESS_KEY**= *YOUR AWS ACCESS KEY*
- **AWS_SECRET_KEY**= *YOUR AWS SECRET KEY*

To prepare the environment, you could type **./marmita.sh** at your command line, to be possible starts all the Terraform plan. 

## The repository content

The repository is divided in specific folders for everything that we need to do:

- **CLUSTER**           - Where the Terraform plans for cluster, vpc, security groups ALB, External DNS and Roles/Policies are configured and provisioned.
- **UTILS**             - Folder that contains some utilities for the cluster, as Prometheus/Grafana/Thanos/Gitlab Runner deployments for the cluster. It hosts some images too (jpeg and gif) used for documentation.

The main script that creates the entire environment is the following, located at root path of the repository:

- **MARMITA.SH**        - Bash script that starts all the Terraform process, creating all the environment.

## How the script works

The idea is simple: a Terraform plan will be started from ***cluster*** folder, that will define an **EKS cluster** + **VPC**, **Security Groups** and  **IAM Roles / Policies**, and after concluded, it will starts the **ALB Ingress Controller** and the **ExternalDNS** setup, that uses **aws cli** and **eksctl** tools. It configures your ***.kube/config*** file, for the environment management command line. The idea at the end is to have an EKS cluster enabled and totally functional, with the **Gitlab CI/CD** runner configured and ready.

## Monitoring metrics

**[NOT FINISHED]** I was working to configure a Grafana and a Prometheus instance (with Thanos to store all logs from Prometheus at a s3 bucket). 

# References

- **Provisioning Kubernetes clusters on AWS with Terraform and EKS**
    - https://learnk8s.io/terraform-eks
    - https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs
- **Provision an EKS Cluster (AWS)**
    - https://learn.hashicorp.com/tutorials/terraform/eks
- **Controlling access to AWS resources using policies**
    - https://docs.aws.amazon.com/IAM/latest/UserGuide/access_controlling.html
    - https://docs.aws.amazon.com/eks/latest/userguide/enable-iam-roles-for-service-accounts.html
    - https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html
    - https://kulasangar.medium.com/creating-and-attaching-an-aws-iam-role-with-a-policy-to-an-ec2-instance-using-terraform-scripts-aa85f3e6dfff
- **External DNS / Route53 / Ingress / ALB**
    - https://aws.amazon.com/premiumsupport/knowledge-center/eks-alb-ingress-controller-fargate/
    - https://artifacthub.io/packages/helm/aws/aws-load-balancer-controller
    - https://github.com/kubernetes-sigs/external-dns/blob/master/docs/tutorials/aws.md
    - https://www.padok.fr/en/blog/external-dns-route53-eks
    - https://www.padok.fr/en/blog/application-load-balancer-aws
- **Gitlab CI/CD**
    - https://imasters.com.br/devsecops/gitlab-ci-seu-proprio-runner-privado-com-docker-compose
    - https://opensource.com/article/20/5/helm-charts
- **Intro to Thanos: Scale Your Prometheus Monitoring With Ease - Lucas Serven & Dominic Green**
    - https://www.youtube.com/watch?v=m0JgWlTc60Q
- **Thanos**
    - https://thanos.io/quick-tutorial.md/
    - https://github.com/thanos-io/thanos
- **Thanos training**
    - https://katacoda.com/thanos/courses/thanos
- **Thanos articles**
    - https://medium.com/airy-science/long-term-and-scalable-ha-prometheus-clusters-at-airy-a306d5b77ffd
    - https://medium.com/uswitch-labs/making-prometheus-more-awesome-with-thanos-fbec8c6c28ad
    - https://medium.com/@kakashiliu/deploy-prometheus-operator-with-thanos-60210eff172b

I hope you like it!!! 

![Logo](./utils/figures/high-five.gif)