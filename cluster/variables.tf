variable "cluster_name" {
  default = "cluster-leoieggli"
}

variable "domain_name" {
  default = "example.net."
}

variable "vpc_id" {
  default = "vpc-leoieggli"
}

variable "region" {
  default     = "us-east-1"
}