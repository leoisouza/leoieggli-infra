# THANOS

Thanos is a CNCF solution for Prometheus long-term retention, high availablity and metric centralization. 


![Thanos architecture](./_pictures/thanos-stack.jpg)

Where:

- Sidecar: connects to Prometheus, reads its data for query and/or uploads it to cloud storage.
- Querier/Query: implements Prometheus’ v1 API to aggregate data from the underlying components.
- Bucket: cloud storage to store metrics (AWS S3, Google Cloud Storafe, Ceph, etc)
- Store Gateway: serves metrics inside of a cloud storage bucket.
- Compactor: compacts, downsamples and applies retention on the data stored in cloud storage bucket.


## How to deploy

**[NOT FINISHED]**


![Thanos options](./_pictures/thanos-options.png)


- Deploy Thanos Querier which is able to talks to Prometheus Sidecar through a gossip protocol.

```
kubectl apply -f deployments/ -n prometheus
```

- Make sure Thanos Sidecar is able to upload Prometheus metrics to the given S3 bucket.
- Establish the Thanos Store for retrieving long term storage.

## References

- **Intro to Thanos: Scale Your Prometheus Monitoring With Ease - Lucas Serven & Dominic Green**
    - https://www.youtube.com/watch?v=m0JgWlTc60Q
- **Thanos**
    - https://thanos.io/quick-tutorial.md/
    - https://github.com/thanos-io/thanos
- **Thanos training**
    - https://katacoda.com/thanos/courses/thanos
- **Thanos articles**
    - https://medium.com/airy-science/long-term-and-scalable-ha-prometheus-clusters-at-airy-a306d5b77ffd
    - https://medium.com/uswitch-labs/making-prometheus-more-awesome-with-thanos-fbec8c6c28ad
    - https://medium.com/@kakashiliu/deploy-prometheus-operator-with-thanos-60210eff172b
